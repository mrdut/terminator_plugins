#!/usr/bin/python

# -*- coding: utf-8 -*-

import terminatorlib.plugin as plugin
import os, re

AVAILABLE = ['PythonExceptionURLHandler']

# print("terminator plugins :",AVAILABLE)

# Important!
# For this plugin to work, the terminator python lib must be modified a bit
# In /usr/lib/python2.7/site-packages/terminatorlib/terminal.py, line 1456,
#
# Replace
# newurl = urlplugin.callback(url)
# with
# newurl = urlplugin.callback(url, self)


class PythonExceptionURLHandler(plugin.URLHandler):
    capabilities = ['url_handler']
    handler_name = 'subl'

    # File "/work-crypt/fdutrech/workspace/bluecime/extra-eyes/scripts/tools/python/static_analysis/log.py", line 390
    # match <FILENAME>:<LINE>
    #match = 'File "([\w/\-_.]+)", line ([0-9]*)'
    match = 'File "([\w/_.-]+)", line ([0-9]*)'

    def callback(self, url, cwd):

        #
        # create file with "fake" extension *.st3
        #
        # *.st3 can be opened using the following bash script
        # ---------------------------
        # #! /usr/bin/bash
        # if [ $# == 1 ]; then subl $(cat $1); fi
        # ---------------------------
        # (MIMETYPE : use right click on dumb .st3 file, choose this script to open by default)

        filename = '/tmp/heyho.st3'

        with open(filename, 'w') as f:
            # write "/work-crypt/fdutrech/workspace/bluecime/extra-eyes/scripts/tools/python/static_analysis/log.py:390"

            match = re.search( PythonExceptionURLHandler.match , url)

            if os.path.isfile(match.group(1)):
                f.write( match.group(1) + ":" + match.group(2) )
            else:
                f.write( os.path.join(cwd, match.group(1)) + ":" + match.group(2) )

        return filename

if __name__ == '__main__':

    python_exception = [

      """Traceback (most recent call last):
          File "scripts/tools/python/static_analysis/main.py", line 7, in <module>
            from rules.evaluation import *
          File "/work-crypt/fdutrech/workspace/bluecime/extra-eyes/scripts/tools/python/static_analysis/rules/evaluation.py", line 20, in <module>
            Rules["eval"]["jobFileNameFormat"]              = Rule("critical", "job file end with '.json'")
          File "/work-crypt/fdutrech/workspace/bluecime/extra-eyes/scripts/tools/python/static_analysis/log.py", line 390, in __init__
            raise Exception("trello_subtitle must be set for urgent rules - it is used in checklist")
        Exception: trello_subtitle must be set for urgent rules - it is used in checklist
        """
        ]

    for e in python_exception:
        print( "-----------------------")
        # print (e)
        # print( "-----------------------")
        for line in e.split('\n'):
            print line
            match = re.search( PythonExceptionURLHandler.match , line)
            if match is not None:
                for i,g in enumerate( match.groups() ):
                    print( i, g )
            else:
                print "-> no match"
        print( "-----------------------\n\n")
