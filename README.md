# INSTALL

```
ln -s goto_last_command.py ~/.config/terminator/plugins
```


# LINKS
 - /usr/share/terminator/plugins
 - [https://developer.gnome.org/vte/unstable/VteTerminal.html#VteTerminal.signals]
 - [http://terminator-gtk3.readthedocs.io/en/latest/plugins.html]
 - [https://github.com/papajoker/git_terminator/blob/master/git_plugin.py]
 - [https://www.snip2code.com/Snippet/58595/Terminator-plugin----log-the-output-of-t]
