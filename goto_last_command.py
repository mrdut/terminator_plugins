#!/usr/bin/python

import os
import sys
import gtk
import terminatorlib.plugin as plugin
from terminatorlib.translation import _

AVAILABLE = ['GotoLastCommand']


class GotoLastCommand(plugin.MenuItem):

    config = None

    def __init__(self):
        plugin.MenuItem.__init__(self)
        if not self.config:
            self.config = {}

        # print "GotoLastCommand init"

    def callback(self, menuitems, menu, terminal):

        vte_terminal = terminal.get_vte()

        # on first call, enable plugin
        if not self.config.has_key(terminal):
            item = gtk.MenuItem(_('Enable  "goto last command"'))
            menuitems.append(item)
            item.connect("activate", self.start_register_position, terminal)

        # otherwise, enable to go to history...
        else:
            item = gtk.MenuItem(_('Goto last command...'))

            if self.config[terminal]["current"] == 0:
                item.set_sensitive(False)
            else:
                item.connect("activate", self.goto_last_command, terminal)
            menuitems.append(item)


    def goto_last_command(self, menu_item, terminal):
        # print "goto_last_command", self.config[terminal]
        self.config[terminal]["current"] -= 1
        index = self.config[terminal]["current"]
        row = self.config[terminal]["history"][index]
        terminal.scrollbar_jump( row )

    def start_register_position(self, menu_item, terminal):
        # print " start register position"
        self.config[terminal] = {}

        row = terminal.get_vte().get_cursor_position()[1]
        self.config[terminal]["history"] = [row]
        self.config[terminal]["current"] = 0
        self.config[terminal]["enable"] = False

        vte_terminal = terminal.get_vte()

        #  will be called on key stroke
        vte_terminal.connect('commit', self.enable_register_position, terminal)

        #  will be called when vte content changed
        vte_terminal.connect('contents-changed', self.register_position, terminal)

        self.register_position(vte_terminal, terminal)

    def register_position(self, vte_terminal, terminal):
        # if self.config.has_key(terminal) and self.config[terminal]["enable"] is True:
        if self.config[terminal]["enable"] is True:
            row = terminal.get_vte().get_cursor_position()[1]
            self.config[terminal]["history"][-1] = row
            # print 'register ', self.config[terminal]
        # else:
        #     print 'ignore register'

    def enable_register_position(self, vte_terminal, text, size, terminal):

        # on 'enter' key stroke
        if size == 1 and ord(text[0])==13:
            self.config[terminal]["enable"] = True
            row = terminal.get_vte().get_cursor_position()[1]
            self.config[terminal]["history"].append( row )

            if len(self.config[terminal]["history"]) > 20:
                self.config[terminal]["history"] = self.config[terminal]["history"][-20:]

            self.config[terminal]["current"] = len(self.config[terminal]["history"])-1

            # print "enable register new row"
        else:
            self.config[terminal]["enable"] = False