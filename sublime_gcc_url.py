#!/usr/bin/env python2

# -*- coding: utf-8 -*-
# Terminator GCC url handler (provides clickable links in gcc error messages) by Florent Dutrech

# Important!
# For this plugin to work, the terminator python lib must be modified a bit
# In /usr/lib/python2.7/site-packages/terminatorlib/terminal.py, line 1456,
#
# Replace
# newurl = urlplugin.callback(url)
# with
# newurl = urlplugin.callback(url, self)


import os

#GCC_MATCH = '([\w/_.-]+\.(cpp|c|hpp|h|hh|cc)):([0-9]+)'
GCC_MATCH = '(\S*)\.(cpp|c|hpp|h|hh|cc):([0-9]+):?([0-9]+)'


if __name__ != '__main__':

    # Imports
    import terminatorlib.plugin as plugin
    from terminatorlib.util import dbg, err

    # Every plugin you want Terminator to load *must* be listed in 'AVAILABLE'
    AVAILABLE = ['GccURLHandler', 'JsonURLHandler', 'AnyFileURLHandler']

    class GccURLHandler(plugin.URLHandler):
        capabilities = ['url_handler']
        handler_name = 'gcc_url'

        # match = '(\/.+\.(?:cpp|c|hpp|h|hh|cc)):([0-9]+)'
        # match = '(/.+\.(?:cpp|c|hpp|h|hh|cc)):([0-9]+)'

        match = GCC_MATCH

        def callback(self, url, cwd):

            filename = '/tmp/heyho.st3'

            print(self)
            print(url)
            print(cwd)

            with open(filename, 'w') as f:

                if url.startswith('/'):
                    f.write(url)
                else:
                    f.write(cwd)
                    f.write("/")
                    f.write(url)

            return filename


    class JsonURLHandler(plugin.URLHandler):
        capabilities = ['url_handler']
        handler_name = 'json_url'
        match = '([\w\d/]+\.json)'

        def callback(self, url, cwd):

            filename = '/tmp/heyho.st3'

            dbg("url" + str(type(url)) + str(url))
            dbg("cwd" + str(type(cwd)) + str(cwd))

            with open(filename, 'w') as f:

                if url.startswith('/'):
                    f.write(url)
                else:
                    f.write(cwd)
                    f.write("/")
                    f.write(url)

            return filename


    class AnyFileURLHandler(plugin.URLHandler):
        capabilities = ['url_handler']
        handler_name = 'any_file_url'

        # match = '[a-zA-Z0-9_/-]+'
        match = '[a-zA-Z0-9_/-]+[.]{1}[a-zA-Z_]+\s'

        def callback(self, url, cwd):

            if os.path.isfile(url):
                return url.rstrip()
            else:
                return os.path.join(cwd, url).rstrip()





    # #!/usr/bin/env python2

    # import terminatorlib.plugin as plugin
    # import re

    # AVAILABLE = ['SublimeGccURLHandler']

    # print("terminator plugins :",AVAILABLE)

    # class SublimeGccURLHandler(plugin.URLHandler):
    #     capabilities = ['url_handler']
    #     handler_name = 'subl'


    #     # match <FILENAME>:<LINE>
    #     match = '(\/.+\.(?:cpp|c|hpp|h|hh|cc)):([0-9]+)'

    #     # def __init__(self):
    #     #     plugin.URLHandler.__init__(self)
    #     #     print("SublimeGccURLHandler")

    #     def callback(self, url):

    #         #
    #         # create file with "fake" extension *.st3
    #         #
    #         # *.st3 can be opened using the following bash script
    #         # ---------------------------
    #         # #! /usr/bin/bash
    #         # if [ $# == 1 ]; then subl $(cat $1); fi
    #         # ---------------------------
    #         # (MIMETYPE : use right click on dumb .st3 file, choose this script to open by default)

    #         filename = '/tmp/heyho.st3'

    #         # print url
    #         with open(filename, 'w') as f:
    #             # write "/work-crypt/fdutrech/workspace/bluecime/extra-eyes/bin/blue-ski/blueski.cpp:27"
    #             f.write(url)

    #         return filename

    # if __name__ == '__main__':

    #     gcc_errors = [

    #         ]

    #     for gcc in gcc_errors:
    #         print( "-----------------------")
    #         print (gcc)
    #         print( "-----------------------")
    #         for i,g in enumerate( re.search( SublimeGccURLHandler.match , gcc).groups() ):
    #             print( i, g )
    #         print( "-----------------------\n\n")

else:
    import re

    tests = []
    tests += ['/work-crypt/fdutrech/workspace/bluecime/extra-eyes/bin/blue-ski/blueski.cpp:5:10: error: missing terminating " character [-Werror]']
    tests += ['                   /work-crypt/fdutrech/workspace/bluecime/extra-eyes/bin/blue-ski/blueski.cpp:164']


    for t in tests:
        print(t)
        print(re.match(GCC_MATCH, t).groups())
